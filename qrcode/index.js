import React from 'react';
import RootT from './routes';
import {UIManager} from 'react-native';
import {Provider as PaperProvider} from 'react-native-paper';

export default class App extends React.Component {

 constructor(props) {
    super(props);
    UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);
  }

  render() {
    return	<PaperProvider>  

    		<RootT />

    		</PaperProvider>
    
  }
}
