import {StackNavigator,createStackNavigator} from 'react-navigation';
import QRScanner from './../screens/qrscanner';
import WebBrowser from './../screens/webbrowser';
import HomeScreen from './../screens/home';
export default RootStack = createStackNavigator({ 
	home: {
		screen:HomeScreen,
		title:"Home",
	},
  	qrscanner:{
  			screen:QRScanner,
  		   },
  	webbrowser:{
  		screen:WebBrowser
  	}

},{headerMode:'none'});


exports 