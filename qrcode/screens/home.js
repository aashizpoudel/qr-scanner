import React,{Component} from 'react';
import {View,Linking,StyleSheet,TouchableOpacity} from 'react-native';
import {Divider,Surface,Title,Text,Button,Headline,Caption} from 'react-native-paper';
import Icon from 'react-native-vector-icons/FontAwesome5';



export default class Root extends Component {

  onScan = e=>{
    this.props.navigation.navigate('qrscanner');
  }	


	render(){
		return  <View style={styles.container}>
                <View style={styles.main}>
                  <Headline style={styles.text}>Welcome</Headline>
                  <Icon name={'home'} style={styles.home} solid/>
                  <Surface style={styles.surface}>
                      <TouchableOpacity><Button onPress={this.onScan} style={styles.button} mode='contained' title='Scan QR Code'>Scan QR Code <Icon name={'search'} solid/></Button></TouchableOpacity>
                  </Surface>
                </View>
                <View style={{flex:1,justifyContent:'flex-end'}}>
                  <Caption style={styles.textRight}>Made with <Icon name={'heart'} color='purple' solid/> By Aashiz</Caption>
                </View>
            </View>
	}
}


const styles = StyleSheet.create({
  container:{
    backgroundColor:'#b0d0d3',
    flex:1,
    justifyContent:'center',
  },
  main:{
    flex:3,
    padding:40,
    justifyContent:'flex-end',
  },
  surface:{
    padding:8,
    height:120,
    marginBottom:10,
    alignItems:'center',
    justifyContent:'center',
    elevation:4,
    backgroundColor:'white'
  },
  button:{
    padding:5,
  },
  text:{
    textAlign:'center',
    // fontSize:20,
    marginBottom:5,
    color:'#c08497',
    // fontWeight:'bold',
    // fontFamily:'Helvetica'
  },
  home:{
    textAlign:'center',
    fontSize:30,
    padding:5,
    color:'#c08497'
  },
  textRight:{
    padding:5,
    textAlign:'right',
    color:'navy'
  }
});
