import React,{Component} from 'react';
import {View,Linking,StyleSheet,TouchableOpacity,AppState,ActivityIndicator} from 'react-native';
import {Text} from 'react-native-paper';
import QRCodeScanner from 'react-native-qrcode-scanner';




export default class Root extends Component {

  constructor(props) {
    super(props);
  
    this.state = {
      hide:true
    };
  }

	onSuccess = e=>{
		Linking.openURL(e.data)
		.catch(err=>{console.log(err);})
	}

  appStateHandler(nextState){
    if(nextState===/inactive|background/){
      this.props.navigation.goBack();
    }
  }

  componentDidMount(){
    AppState.addEventListener('change',this.appStateHandler);
    setTimeout(()=>{
      this.setState({hide:false})
    },500);
  }

  componentWillUnmount(){
    AppState.removeEventListener('change',this.appStateHandler);
  }


	render(){
		return <View style={{flex:1}}> 
        { this.state.hide ?<ActivityIndicator style={{flex:1}}/>:null }
      {this.state.hide ? null: 
        <QRCodeScanner style={{visible:'none'}} containerStyle={{backgroundColor:'#b0d0d3'}} topViewStyle={{}} bottomViewStyle={{paddingTop:20}}
        onRead={this.onSuccess}
        topContent={
          <Text style={styles.centerText}>
            Scan the desired QR Code!!
          </Text>
        }
        cameraStyle={styles.camera}
        bottomContent={
          <TouchableOpacity onPress={()=>{this.props.navigation.goBack()}} style={styles.buttonTouchable}>
            <Text style={styles.buttonText}>Cancel</Text>
          </TouchableOpacity>
        }
      />

      }

      </View>
	}
}
const styles = StyleSheet.create({
  centerText: {
    flex: 1,
    fontSize: 18,
    padding: 25,
    color: '#777',
  },
  textBold: {
    fontWeight: '500',
    color: '#000',
  },
  buttonText: {
    fontSize: 21,
    color: 'rgb(0,122,255)',
  },
  buttonTouchable: {
    padding: 1,
  },
  
});

